[ActivityPub](http://activitypub.rocks/) is the protocol used by federated social networks like Mastodon, GNU Social, etc. to achieve federation among servers.
Mastodon is one kind of social network application in this federated social network and it provides an option to download the archive of a user's profile information and toots (i.e posts).

However, the downloaded archive is a combination of files and folders. User's profile information are stored in `actors.json` file and rest of the activities of user is stored in `outbox.json` file.
They both are stored in the format as designed in the ActivityPub protocol, therefore a user can simply upload the downloaded archive to another mastodon instance if they wish to migrate their account
from one server to another server.

This program helps users to make sense of these data, in case if they wish to download their copy and take a look at them.

1. Download your archive from your mastodon instance.
2. Uncompress the downloaded archvie.
3. Download this repo.

    python main.py /path/to/archive-folder/

**TODO**:
- [] Add argument parser
- [] Render as HTML
  - [] Design a Template
