class NotAnActor(Exception):
    '''Custom Exception class for Actor'''
    def __str__(self):
        return "Actor can only be created with actor data."


class NotAnOutbox(Exception):
    '''Custom Exception class for Outbox'''
    def __str__(self):
        return "Outbox can only be created with outbox data."


class Actor:
    '''creates and returns an Actor object'''

    def __init__(self, json_data):
        self.account_type = json_data.get('type', None)

        if self.account_type:
            if (self.account_type != 'Person') and (self.account_type != 'Bot'):
                raise NotAnActor()

            self.name = json_data.get('name', None)
            self.username = json_data.get('preferredUsername', None)
            self.summary = json_data.get('summary', None)
            self.image = json_data.get('image', None)

            self.human = False
            if self.account_type == "Person":
                self.human = True
        else:
            raise NotAnActor()

    def __repr__(self):
        if self.human:
            message = "This account belongs to {}.".format(self.name)
        else:
            message = "{} is a bot account.".format(self.name)
        return message


class Outbox:
    '''creates and returns an Outbox object'''

    def __init__(self, json_data):
        self.object_type = json_data.get('type', None)

        if self.object_type:
            if json_data.get('type') != 'OrderedCollection':
                raise NotAnOutbox()

            self.total_items = json_data.get('totalItems', 0)
            self.items = json_data.get('ordredItems', [])
        else:
            raise NotAnOutbox()

    def __repr__(self):
        return "Total Items in Outbox: {}".format(self.total_items)
