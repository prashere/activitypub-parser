#!/usr/bin/env python

import json
import sys

from activitypub import Actor, Outbox

def main():
    # read from json file
    actor_json = json.loads(open(ACTOR, 'r').read())
    outbox_json = json.loads(open(OUTBOX, 'r').read())

    # create actor object
    actor = Actor(actor_json)
    print (actor)

    # create outbox object
    outbox = Outbox(outbox_json)
    print (outbox)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print ("supply a path to your uncompressed archive folder")
        exit()
    else:
        ARCHIVE_FOLDER = sys.argv[1]
        ACTOR = ARCHIVE_FOLDER + '/actor.json'
        OUTBOX = ARCHIVE_FOLDER + '/outbox.json'
        main()
